# Rusty Roguelike

Little Rogue Like made in Rust using the [guide](http://bfnightly.bracketproductions.com/rustbook/chapter_0.html).

## Lecture

- [Introduction](http://bfnightly.bracketproductions.com/rustbook/chapter_0.html)
- [Section 1](http://bfnightly.bracketproductions.com/rustbook/chapter_1.html)
  - [Entities and component](http://bfnightly.bracketproductions.com/rustbook/chapter_2.html)
  - [Walking a Map](http://bfnightly.bracketproductions.com/rustbook/chapter_3.html)
  - [A more interesting map](http://bfnightly.bracketproductions.com/rustbook/chapter_4.html)
  - [Field of view](http://bfnightly.bracketproductions.com/rustbook/chapter_5.html)
  - [Monster](http://bfnightly.bracketproductions.com/rustbook/chapter_6.html)
  - [Dealing damage](http://bfnightly.bracketproductions.com/rustbook/chapter_7.html)
